// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyD4lJdYPM_LmDJTAfnay-5lrkHl5-p6hBA",
    authDomain: "ex4-new.firebaseapp.com",
    databaseURL: "https://ex4-new.firebaseio.com",
    projectId: "ex4-new",
    storageBucket: "ex4-new.appspot.com",
    messagingSenderId: "640176335988"
  }
};
 
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
